using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilEne : MonoBehaviour
{
    public ProyectilBaseSO TipoProyectill;
    public float spdE;
    public float dmgE;
    public Vector2 destino;
    private string comportamiento;
    void Start()
    {
        comportamiento = TipoProyectill.comportamiento;
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(destino.x - this.GetComponent<Transform>().position.x, destino.y - this.GetComponent<Transform>().position.y).normalized *15f;
        if (comportamiento == "critic" && Random.Range(0, 5) == 1) { dmgE += 2; }
    }

    private void Update()
    {
        if (this.GetComponent<SpriteRenderer>().isVisible == false) Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Torre")
        {
            collision.GetComponent<Torre>().getDamage(dmgE+TipoProyectill.dmg);
            Destroy(this.gameObject);
        }
    }
}
