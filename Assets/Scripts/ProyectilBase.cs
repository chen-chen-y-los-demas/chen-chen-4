using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;


public class ProyectilBase : MonoBehaviour
{
    public ProyectilBaseSO tipoProyectil;
    public Vector3 destino;
    public float dmgT;
    public float spdT;
    private string comportamiento;
    public GameObject torre;
    void Start()
    {
        comportamiento = tipoProyectil.comportamiento;
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(destino.x - this.GetComponent<Transform>().position.x, destino.y - this.GetComponent<Transform>().position.y).normalized*(tipoProyectil.spd+this.spdT);
        if (comportamiento == "critic" && Random.Range(0, 5)==1) { dmgT += 2; }
    }

    private void Update()
    {
        if (this.GetComponent<SpriteRenderer>().isVisible == false) Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Enemigo")
        {
            bool muerto = collision.GetComponent<Enemigo>().getDamage(tipoProyectil.dmg + this.dmgT);
            if (muerto) torre.GetComponent<Torre>().ganarExp(collision.GetComponent<Enemigo>().expOtorgada);
            Destroy(this.gameObject);
        }
    }
}
