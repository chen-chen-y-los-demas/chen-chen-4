using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    
    public void Game()
    {
        SceneManager.LoadScene("eleccionInicial", LoadSceneMode.Single);
    }

    public void Ayuda()
    {
        SceneManager.LoadScene("Ayuda", LoadSceneMode.Single);
    }

    public void Ajustes()
    {
        SceneManager.LoadScene("Ajustes", LoadSceneMode.Single);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
