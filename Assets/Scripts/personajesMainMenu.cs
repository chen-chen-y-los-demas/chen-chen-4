using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class personajesMainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public RuntimeAnimatorController ControlerAnim;
    void Start()
    {

        this.GetComponent<Animator>().runtimeAnimatorController = ControlerAnim;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<SpriteRenderer>().isVisible == false && this.transform.position.x > 0) this.gameObject.SetActive(false);
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(2, 0, 0);
    }
}
