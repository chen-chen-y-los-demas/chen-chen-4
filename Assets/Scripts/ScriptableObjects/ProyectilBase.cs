using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu]
public class ProyectilBaseSO : ScriptableObject
{
    public float spd;
    public int dmg;
    public string comportamiento;
    // esta forma de tratarlo deberia de cambiar para optimizar, pero para hacerlo rapido de momento mejor asi
    // comportamientos permitidos:
    // critic: Permite hacer criticos (aleatoriamente hace da�o)
    // expo: Da�o exponencial (el da�o aumenta aun mas cada nivel)
    // block: Ralentiza enemigos area (solo torres)
    // stop: bloquea el movimiento enemigo (solo torres)

}
