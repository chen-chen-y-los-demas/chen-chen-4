using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MokeponInventory : MonoBehaviour
{
    public GameObject[] mokepons;
    InvMokepon invMok;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void asignarMokepon(Enemigo newMokepon) {
        foreach (GameObject go in mokepons) {
            invMok = go.GetComponent<InvMokepon>();
            if (!invMok.TieneMokepon) {
                invMok.TieneMokepon = true;
                invMok.mokepon.hp_max = newMokepon.torrePrefab.GetComponent<Torre>().hp_max;
                invMok.mokepon.hp = newMokepon.torrePrefab.GetComponent<Torre>().hp_max;
                invMok.mokepon.dmg = newMokepon.torrePrefab.GetComponent<Torre>().dmg;
                invMok.mokepon.velDisparo = newMokepon.torrePrefab.GetComponent<Torre>().velDisparo;
                invMok.mokepon.municion = newMokepon.torrePrefab.GetComponent<Torre>().municion;
                invMok.mokepon.nivel = newMokepon.nivel;
                invMok.mokepon.cansado = false;
                invMok.torrePrefab = newMokepon.torrePrefab;
                go.GetComponent<SpriteRenderer>().sprite = newMokepon.GetComponent<SpriteRenderer>().sprite;
                break;
            }
        }
    }

    public bool TeamLleno()
    {
        bool comprovar = true;
        foreach (GameObject go in mokepons)
        {
            if (!go.GetComponent<InvMokepon>().TieneMokepon)
            {
                comprovar = false;
            }
        }
        return comprovar;
    }
}
