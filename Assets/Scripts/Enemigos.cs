using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

public class Enemigo : Mokepon
{
    public float spdd;
    public bool capturable;
    public int expOtorgada;
    
    private int llendoA;
    public int NeuronasNum; // cuando mas peque�o mas listo XD
    public float NeuronasSpeed; // cuando mas peque�o tambien mas listo XD

    public List<GameObject> torres = new List<GameObject>();
    public bool apuntando;
    public bool momentoCaptura; //solo se podria poder capturar cuando sea true

    public delegate List<GameObject> Actualizar();
    public event Actualizar Actualizando;

    public GameObject torrePrefab;
    public GameObject NodeManagerGameObject;
    public GameObject mokInventory;
    public GameObject textMoney;

    public float mult;

    // Start is called before the first frame update
    void Start()
    {
        expOtorgada = expOtorgada*nivel;
        int random = Random.Range(1, 4);
        if (random == 1) capturable = true;
        else capturable = false;
        this.llendoA = 0;
        apuntando = false;
        momentoCaptura = false;
        cansado = false;
        mult = 1;

        // variables del scriptable object

        StartCoroutine(pensar());
    }

    // Update is called once per frame

    private void FixedUpdate()
    {
        canvioDireccion();
    }

    public override void subirNivel(int cant)
    {
        base.subirNivel(cant);
        if (this.municion.GetComponent<ProyectilEne>().TipoProyectill.comportamiento == "expo") this.dmg += 1 * cant;
    }

    public void capturar()
    {
        if (momentoCaptura == true)
        {
            print("capturar");
            mokInventory.GetComponent<MokeponInventory>().asignarMokepon(this);
            Destroy(this.gameObject);
        }
    }

    public bool getDamage(float dmg) 
    {
        bool muerto = false;
        hp += (int)-dmg;
        this.barraDeVida.GetComponent<RectTransform>().sizeDelta = new Vector3(hp / (float)hp_max * 85, 15, 0);
        if (hp <= 0 && !cansado)
        {
            muerto = true;
            print("muero");
            ganarDinero();
            this.GetComponent<SpriteRenderer>().flipX = false;
            StartCoroutine(morir());
        }
        return muerto;
    }

    public IEnumerator morir() 
    {
        cansado = true;
        if (capturable == true)
        {
            momentoCaptura = true;
            this.GetComponent<Animator>().SetBool("cansado", true);
            yield return new WaitForSeconds(NeuronasSpeed*15);
            this.GetComponent<Animator>().SetBool("cansado", false);
            momentoCaptura = false;
        }
        while (this.GetComponent<SpriteRenderer>().isVisible)
        {
            print(this.spdd);
            this.GetComponent<Transform>().Translate((new Vector3(20, 20, 0) - this.GetComponent<Transform>().position).normalized * (this.spdd/10) /250f * mult);
            yield return new WaitForSeconds(0.001f);
        }
        Destroy(this.gameObject);
    }
    public void canvioDireccion()
    {
        if (apuntando == false && cansado == false) 
        {
            Vector3 llendo = NodeManagerGameObject.GetComponent<NodeManager>().nodosM.nodos[llendoA].GetComponent<Transform>().position;
            float spdx = llendo.x - this.GetComponent<Transform>().position.x;
            float spdy = llendo.y - this.GetComponent<Transform>().position.y;
            if (spdx > 0)
            {
                this.GetComponent<SpriteRenderer>().flipX = false;
            }
            else {
                this.GetComponent<SpriteRenderer>().flipX = true;
            }
            if (Vector3.Distance(this.GetComponent<Transform>().position, llendo) < 0.1f) this.llendoA += 1;
            if (this.llendoA > NodeManagerGameObject.GetComponent<NodeManager>().nodosM.nodos.Length - 1) this.llendoA = 0;
            this.GetComponent<Transform>().Translate((new Vector3(llendo.x, llendo.y, 0) - this.GetComponent<Transform>().position).normalized * (this.spdd/10) / 250f * mult);
        }
        else { this.GetComponent<Rigidbody2D>().velocity = Vector3.zero; }
    }

    public void ganarDinero() {
        SceneController.totalMoney += (int) (nivel*0.5);
    }

    public IEnumerator pensar() 
    {
        while (true)
        {
            yield return new WaitForSeconds(NeuronasSpeed);
            if (cansado == true) break;
            if (Random.Range(0, NeuronasNum) == 0) 
            {
                torres = Actualizando.Invoke();
                if (torres.Count != 0)
                {
                    int torre = Random.Range(0, torres.Count-1);
                    if (torres[torre].GetComponent<Torre>().cansado == false) 
                    {
                        apuntando = true;
                        this.GetComponent<Animator>().SetBool("disparando", true);
                        yield return new WaitForSeconds(NeuronasSpeed);
                        var disparo = Instantiate(municion);
                        disparo.transform.position = transform.position;
                        var proyectil = disparo.GetComponent<ProyectilEne>();
                        proyectil.destino = (Vector2)torres[torre].transform.position;
                        proyectil.spdE = velDisparo;
                        proyectil.dmgE = dmg;
                        yield return new WaitForSeconds(NeuronasSpeed);
                        apuntando = false;
                        this.GetComponent<Animator>().SetBool("disparando", false);
                    }
                }
            }
        }
    }
}

