using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using TMPro;
using Unity.VisualScripting.Antlr3.Runtime.Tree;
using UnityEngine;
using static UnityEngine.UIElements.UxmlAttributeDescription;

public class ObjetoMochila : MonoBehaviour
{
    public string nom;
    public int quantity;

    public GameObject mokInventory;
    private Collider2D target;
    private bool drag = false;
    private Vector2 originalPos;
    public GameObject textQuantity;
    public GameObject sceneController;
    SceneController sc;

    void Start()
    {
        originalPos = GetComponent<Transform>().position;
        nom = null;
        sc = sceneController.GetComponent<SceneController>();
        textQuantity.GetComponent<TextMeshProUGUI>().text = "" + quantity;
    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.transform.tag == "Enemigo") {
            if (collision.GetComponent<Enemigo>().momentoCaptura) {
                target = collision;
            }
        } else if (collision.transform.tag == "Torre")
        {
            target = collision;
        }
            

    }


    

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Torre" || collision.transform.tag == "Enemigo")
        {
            target = null;
        }
    }

    

    public void OnMouseDrag()
    {
        if (nom != null) {
            this.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 00);
            drag = true;
        }

        
    }

    public void OnMouseUp()
    {
        if (target != null)
        {
            if (target.transform.tag == "Torre")
            {
                if (nom == "Carameloraro")
                {
                    print("Da Caramelo");
                    target.GetComponent<Torre>().subirNivel(1);
                    quantity--;
                }
                if (nom == "Potion")
                {
                    Torre torre = target.GetComponent<Torre>();
                    if (torre.hp == torre.hp_max)
                    {
                        print("Tu Mokepon ya esta curado.");
                    }
                    else
                    {
                        quantity--;
                        torre.hp = torre.hp_max;
                        torre.barraDeVida.GetComponent<RectTransform>().sizeDelta = new Vector3(85, 15, 0);
                    }
                }
            }
            else if (target.transform.tag == "Enemigo")
            {
                if (nom == "Pokeball")
                {
                    if (!mokInventory.GetComponent<MokeponInventory>().TeamLleno())
                    {
                        target.GetComponent<Enemigo>().capturar();
                        quantity--;
                    }
                    else
                    {
                        print("team completo");
                    }
                }

            }
        }

        if (quantity <= 0) {
            nom = null;      
        }

        textQuantity.GetComponent<TextMeshProUGUI>().text = "" + quantity;
        this.transform.parent.GetComponent<Mochila>().changeSprite();

        if (drag)
        {
            drag = false;
            GetComponent<Transform>().position = originalPos;
        }
        

    }



}
