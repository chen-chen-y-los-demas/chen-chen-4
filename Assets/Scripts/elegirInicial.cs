using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class elegirInicial : MonoBehaviour
{

    public SOinicial torreInicial;
    public GameObject chenPrefab;
    public GameObject ikerPrefab;
    public GameObject davidPrefab;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void buttonInicial(int num){
        if (num == 0) {
            torreInicial.torreInicial = chenPrefab;
        }
        else if (num == 1)
        {
            torreInicial.torreInicial = ikerPrefab;
        }
        else
        {
            torreInicial.torreInicial = davidPrefab;
        }
        SceneManager.LoadScene("Juego_escena");
    }
}
