using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShopItem : MonoBehaviour
{
    public string nameItem;
    public int numPrice;

    public GameObject textPrice;

    public GameObject mochila;

    void Start()
    {
        textPrice.GetComponent<TextMeshProUGUI>().text = ""+numPrice;
    }

    
    void Update()
    {
        
    }

    public void clickShopItem()
    {
        if (SceneController.totalMoney >= numPrice)
        {
            foreach (GameObject objBack in mochila.GetComponent<Mochila>().ObjectsMochila)
            {
                print(objBack.GetComponent<ObjetoMochila>().nom);
                if (objBack.GetComponent<ObjetoMochila>().nom == nameItem && objBack.GetComponent<ObjetoMochila>().quantity < 9)
                {
                    SceneController.totalMoney -= numPrice;
                    objBack.GetComponent<ObjetoMochila>().quantity++;
                    objBack.GetComponent<ObjetoMochila>().textQuantity.GetComponent<TextMeshProUGUI>().text = ""+objBack.GetComponent<ObjetoMochila>().quantity;
                    break;
                }
                else if (objBack.GetComponent<ObjetoMochila>().nom == null)
                {
                    SceneController.totalMoney -= numPrice;
                    objBack.GetComponent<ObjetoMochila>().quantity++;
                    objBack.GetComponent<ObjetoMochila>().textQuantity.GetComponent<TextMeshProUGUI>().text = "" + objBack.GetComponent<ObjetoMochila>().quantity;
                    objBack.GetComponent<ObjetoMochila>().nom = nameItem;
                    mochila.GetComponent<Mochila>().changeSprite();
                    break;
                }

            }
        }
        else
        {
            print("No tienes suficiente dinero.");
        }
    }
}
