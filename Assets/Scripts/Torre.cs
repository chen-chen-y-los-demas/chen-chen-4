using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Torre : Mokepon
{
    // Start is called before the first frame update
    public int exp;
    public int exp_paraSiguiente;
    public float shootSpd;
    public List<GameObject> enemigosEnRadio = new List<GameObject>();
    public RuntimeAnimatorController ControlerEvo1;
    public RuntimeAnimatorController ControlerEvo2;
    public RuntimeAnimatorController ControlerEvo3;

    public GameObject invMokepon;
    public delegate List<GameObject> Actualizar();
    public event Actualizar Actualizando;

    void Start()
    {
        
        this.cansado = false;
        this.exp = 0;
        this.exp_paraSiguiente = 100;
        this.StartCoroutine(disparar());
        this.GetComponent<Animator>().runtimeAnimatorController = this.ControlerEvo1;
    }

    public override void subirNivel(int cant)
    {
        if (cant > 0)
        {
            base.subirNivel(cant);
            this.exp = 0;
            for (int i=0;i<cant;i++) {
                this.exp_paraSiguiente += (int) (this.exp_paraSiguiente*0.6);
            }
            if (this.municion.GetComponent<ProyectilBase>().tipoProyectil.comportamiento == "expo") this.dmg += 1 * cant;
            if (this.nivel < 10) this.GetComponent<Animator>().runtimeAnimatorController = this.ControlerEvo1;
            else if (this.nivel < 20) this.GetComponent<Animator>().runtimeAnimatorController = this.ControlerEvo2;
            else this.GetComponent<Animator>().runtimeAnimatorController = this.ControlerEvo3;
        } 
    }

    public void ganarExp(int expRecibida) {
        this.exp += expRecibida;
        if (exp >= exp_paraSiguiente)
        {
            subirNivel(1);

        }
    }

    public void getDamage(float dmg)
    {
        
        hp += (int) -dmg;
        this.barraDeVida.GetComponent<RectTransform>().sizeDelta = new Vector3(hp/(float)hp_max*85, 15, 0);
        if (hp <= 0)
        {
            print("muero");
            StartCoroutine(morir());
        }
    }

    private void OnMouseDown()
    {
        print(this.nivel);
    }

    public IEnumerator morir()
    {
        print("entro en funcion");
        cansado = true;
        cambiarInventario();
        while (this.GetComponent<SpriteRenderer>().isVisible)
        {
            this.GetComponent<Transform>().Translate((new Vector3(20, 20, 0) - this.GetComponent<Transform>().position).normalized/100f);
            yield return new WaitForSeconds(0.001f);
        }
        Destroy(this.gameObject);
    }

    public void cambiarInventario() {
        invMokepon.GetComponent<SpriteRenderer>().sprite = null;
        invMokepon.GetComponent<InvMokepon>().TieneMokepon = false;
        invMokepon.GetComponent<InvMokepon>().MokeponEnCombate = false;
    }

    public void maxXP() 
    {
        if (this.exp >= this.exp_paraSiguiente) subirNivel(1);
    }

    public IEnumerator disparar() 
    {
        while (true)
        {
            yield return new WaitForSeconds(shootSpd);
            if (cansado == true) break;
            this.enemigosEnRadio = this.Actualizando?.Invoke();
            if (this.enemigosEnRadio != null && this.enemigosEnRadio.Count != 0)
            {
                int refiero = -1;
                for (int i=0; i<this.enemigosEnRadio.Count; i++)
                {
                    if (enemigosEnRadio[i].GetComponent<Enemigo>().cansado == false)
                    {
                        refiero = i;
                        break;
                    }
                }
                if (refiero != -1) 
                {
                    var disparo = Instantiate(municion);
                    disparo.transform.position = transform.position;
                    var proyectil = disparo.GetComponent<ProyectilBase>();
                    proyectil.destino = enemigosEnRadio[refiero].transform.position;
                    proyectil.spdT = velDisparo;
                    proyectil.dmgT = dmg;
                    proyectil.torre = this.gameObject;
                }
            }
        }
    }
}
