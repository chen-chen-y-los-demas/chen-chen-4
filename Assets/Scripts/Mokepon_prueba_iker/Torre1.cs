using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class Torre1 : Mokepon1
{

    public int exp; 
    public int exp_paraSiguiente;

    void Start()
    {
        this.exp = 0;
        this.exp_paraSiguiente = this.nivel * 10;
    }


    void Update()
    {
        
    }

    public override void subirNivel(int cant)
    {
        print("subenivel");
        base.subirNivel(cant);
        this.exp += - this.exp_paraSiguiente;
        this.exp_paraSiguiente = this.nivel * 10;
    }

    public void maxXP() 
    {
        if (this.exp >= this.exp_paraSiguiente) subirNivel(1);
    }

    private void OnMouseDown()
    {
        print(this.nivel);

    }
}
