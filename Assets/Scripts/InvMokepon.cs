using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class InvMokepon : MonoBehaviour
{
    public GameObject torrePrefab;
    public Torre mokepon;
    public Torre mokeponInicial;
    public SOinicial inicial;

    private bool sacarInicial;
    private Collider2D target;
    private bool drag = false;
    public bool TieneMokepon;
    public bool MokeponEnCombate = false;
    GameObject torre;
    private Vector2 originalPos;
    public Camera camera;
    void Start()
    {
        sacarInicial = false;
        TieneMokepon = false;
        mokepon = new Torre();
        originalPos = GetComponent<Transform>().position;

        if (inicial != null) {
            torrePrefab = inicial.torreInicial;
            mokeponInicial = inicial.torreInicial.GetComponent<Torre>();
            GetComponent<SpriteRenderer>().sprite = torrePrefab.GetComponent<SpriteRenderer>().sprite;
            mokepon.invMokepon = this.gameObject;
            mokepon.hp_max = mokeponInicial.hp_max;
            mokepon.hp = mokeponInicial.hp;
            mokepon.nivel = mokeponInicial.nivel;
            mokepon.dmg = mokeponInicial.dmg;
            mokepon.shootSpd = mokeponInicial.shootSpd;
            mokepon.velDisparo = mokeponInicial.velDisparo;
            mokepon.municion = mokeponInicial.municion;
            TieneMokepon = true;
            sacarInicial = true;
        } 
    }

    void Update()
    {
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Colocar")
        {
            target = collision;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Colocar")
        {
            target = null;
        }
    }

    public void OnMouseDrag()
    {
        if (TieneMokepon && !MokeponEnCombate)
        {
                this.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 00);
                drag = true;
        }   
    }

    public void OnMouseUp()
    {
        if (drag)
        {
            if (target != null) {
                if (target.transform.tag == "Colocar")
                {
                    torre = Instantiate(torrePrefab, new Vector2(this.transform.position.x, this.transform.position.y), transform.rotation);
                    drag = false;
                    MokeponEnCombate = true;
                    GetComponent<Transform>().position = originalPos;
                    Torre tclass = torre.GetComponent<Torre>();
                    tclass.invMokepon = this.gameObject;
                    tclass.hp_max = mokepon.hp;
                    tclass.hp = mokepon.hp;
                    tclass.dmg = mokepon.dmg;
                    tclass.shootSpd = 0.5f;
                    tclass.velDisparo = mokepon.velDisparo;
                    tclass.municion = mokepon.municion;
                    if (sacarInicial) {
                        tclass.subirNivel(4);
                        sacarInicial = false;
                    }
                    tclass.subirNivel(mokepon.nivel-1);
                    tclass.canvas.GetComponent<Canvas>().worldCamera = this.camera;
                    tclass.textoNivel.GetComponent<TextMeshProUGUI>().text = "" + tclass.nivel;
                }
            }
            else
            {
                drag = false;
                GetComponent<Transform>().position = originalPos;
            }

        }
        
    }
}
