using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioEnemigo : MonoBehaviour
{
    public List<GameObject> torres = new List<GameObject>();
    public Enemigo enemigo; // Esto abra que cambiarlo en un futuro para no tener de arrastrar que torre es en su radio

    void Start()
    {
        enemigo.Actualizando += Enviar;
        this.GetComponent<SpriteRenderer>().forceRenderingOff = true;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Torre")
        {
            print("interactuo");
            this.torres.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Torre")
        {
            this.torres.Remove(collision.gameObject);
        }
    }

    public List<GameObject> Enviar()
    {
        return this.torres;
    }
}
