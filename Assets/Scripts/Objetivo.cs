using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Objetivo : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public int hp;
    public GameObject barradevida;
    void Start()
    {
        hp = 1000;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.hp < 0) SceneManager.LoadScene("ChenOver");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Enemigo")
        {
            print("entra");

            this.hp -= collision.gameObject.GetComponent<Enemigo>().hp;
            collision.gameObject.GetComponent<Enemigo>().capturable = false;
            collision.gameObject.GetComponent<Enemigo>().getDamage(collision.gameObject.GetComponent<Enemigo>().hp_max + 1);
            barradevida.GetComponent<RectTransform>().sizeDelta = new Vector3(hp / (float)1000 * 150, 30, 0);
        }
    }
}
