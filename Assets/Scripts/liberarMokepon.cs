using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class liberarMokepon : MonoBehaviour
{

    private Vector2 originalPos;
    private Collider2D target;
    // Start is called before the first frame update
    void Start()
    {
        originalPos = GetComponent<Transform>().position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Torre")
        {

            target = collision;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Torre")
        {
            target = null;
        }
    }

    public void OnMouseDrag()
    {
        this.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 00);
    }

    private void OnMouseUp()
    {
        this.transform.position = originalPos;
        if (target != null) {
            target.GetComponent<Torre>().cambiarInventario();
            Destroy(target.gameObject);
        }
    }
}
