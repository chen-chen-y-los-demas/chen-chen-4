using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIVida : MonoBehaviour
{
    public Objetivo b;
    void Start()
    {
        
    }

    
    void Update()
    {
        if(b.hp <= 0)
        {
            SceneManager.LoadScene("ChenOver", LoadSceneMode.Single);
        }

        this.GetComponent<TextMeshProUGUI>().text = "Vidas : " + b.hp;
    }
}
