using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;

public class Enemigo1 : Mokepon1
{
    public GameObject mokInventory;
    public GameObject NodeManagerGameObject;
    public double spd;
    private bool capturable;
    private int llendoA;


    void Start()
    {
        int random = Random.Range(0, 10);
        if (random == 1) capturable = true;
        else capturable = false;
        this.llendoA = 0;
    }

    void Update()
    {
        canvioDireccion();
    }

    public override void subirNivel(int cant)
    {
        base.subirNivel(cant);
        this.spd += (0.1 * cant);
    }

    public void capturar() 
    {
        if (capturable == true) 
        {
            print("capturar");
            //mokInventory.GetComponent<MokeponInventory>().asignarMokepon(this);
            Destroy(this.gameObject);
            // aqui ira el codigo de captura
            // (he hecho esto porque me parece feo el warning de unity)
        }
    }

    public void canvioDireccion()
    {

        Vector3 llendo = NodeManagerGameObject.GetComponent<NodeManager>().nodosM.nodos[llendoA].GetComponent<Transform>().position;
        float spdx = llendo.x - this.GetComponent<Transform>().position.x;
        float spdy = llendo.y - this.GetComponent<Transform>().position.y;
        if (Vector3.Distance(this.GetComponent<Transform>().position, llendo) < 0.1f) this.llendoA += 1;
        if (this.llendoA > NodeManagerGameObject.GetComponent<NodeManager>().nodosM.nodos.Length - 1) this.llendoA = 0;
        this.GetComponent<Transform>().Translate((new Vector3(llendo.x, llendo.y, 0) - this.GetComponent<Transform>().position).normalized * 0.05f);
    }
}
