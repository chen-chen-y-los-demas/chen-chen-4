using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Mokepon1 : MonoBehaviour
{
    public int hp_max;
    public int hp;
    public double dmg;
    public GameObject municion;
    public double velDisparo;
    public int nivel;
    public bool cansado;

    public GameObject sceneController;
    public SceneController sc;
    public GameObject mochila;

    void Start()
    {
        sc = sceneController.GetComponent<SceneController>();

        this.hp = this.hp_max;
        this.cansado = false;
    }
    void Update()
    {
        
    }

    public virtual void subirNivel(int cant) 
    {
        this.nivel += cant;
        this.hp_max = this.hp_max * 100;
        this.hp = this.hp_max;
        this.dmg += 0.5 * cant;
        this.velDisparo += -(0.1 * cant);
    }

    public virtual void cansarse() 
    {
        if (this.cansado == true) Destroy(this.gameObject);
    }

}
