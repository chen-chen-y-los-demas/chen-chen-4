using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class Mokepon : MonoBehaviour
{
    // Start is called before the first frame update
    public int hp_max;
    public int hp;
    public float dmg;
    public GameObject municion;
    public float velDisparo;
    public int nivel;
    public bool cansado;

    public GameObject textoNivel;
    public GameObject barraDeVida;
    public GameObject canvas;
    public Camera Camera;

    void Start()
    {
        this.nivel = 1;
        this.barraDeVida.GetComponent<RectTransform>().sizeDelta = new Vector3(85, 15, 0);
        this.textoNivel.GetComponent<TextMeshProUGUI>().text = "" + nivel;
        this.hp = this.hp_max;
        this.cansado = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void subirNivel(int cant) 
    {
        for (int i = 0;i<cant;i++) {
            this.nivel++;
            this.textoNivel.GetComponent<TextMeshProUGUI>().text = "" + nivel;
            this.hp_max += (int) (hp_max*0.3);
            this.hp = this.hp_max;
            this.barraDeVida.GetComponent<RectTransform>().sizeDelta = new Vector3(85, 15, 0);
            this.dmg += this.dmg * 0.2f;
        }
        
    }

    public virtual void cansarse() 
    {
        if (this.cansado == true) Destroy(this.gameObject);
    }

}
