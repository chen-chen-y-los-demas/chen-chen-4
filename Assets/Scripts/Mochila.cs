using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mochila : MonoBehaviour
{

    public Sprite[] ObjectSprites;
    public GameObject[] ObjectsMochila;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void changeSprite()
    {
        
        foreach (GameObject objBack in ObjectsMochila)
        {
            if (objBack.GetComponent<ObjetoMochila>().nom == "Carameloraro")
            {
                objBack.GetComponent<SpriteRenderer>().sprite = ObjectSprites[1];
            }
            else if (objBack.GetComponent<ObjetoMochila>().nom == "Potion")
            {
                objBack.GetComponent<SpriteRenderer>().sprite = ObjectSprites[2];
            }
            else if (objBack.GetComponent<ObjetoMochila>().nom == "Pokeball")
            {
                objBack.GetComponent<SpriteRenderer>().sprite = ObjectSprites[3];
            }
            else if (objBack.GetComponent<ObjetoMochila>().nom == null)
            {
                objBack.GetComponent<SpriteRenderer>().sprite = ObjectSprites[0];
            }
        }
            
    }
}
