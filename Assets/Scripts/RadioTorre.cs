using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioTorre : MonoBehaviour
{
    public List<GameObject> enemigos = new List<GameObject>();
    public Torre torre; // Esto abra que cambiarlo en un futuro para no tener de arrastrar que torre es en su radio

    void Start()
    {
        torre.Actualizando += Enviar;
        this.GetComponent<SpriteRenderer>().forceRenderingOff = true;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Enemigo")
        {
            this.enemigos.Add(collision.gameObject);
            if (torre.municion.GetComponent<ProyectilBase>().tipoProyectil.comportamiento == "block") collision.GetComponent<Enemigo>().mult = 0.5f;
            if (torre.municion.GetComponent<ProyectilBase>().tipoProyectil.comportamiento == "stop") collision.GetComponent<Enemigo>().mult = 0.1f;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Enemigo")
        {
            this.enemigos.Remove(collision.gameObject);
            collision.GetComponent<Enemigo>().mult = 1f;
        }
    }

    public List<GameObject> Enviar()
    {
        return this.enemigos;
    }
}
