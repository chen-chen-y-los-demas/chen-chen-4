using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class pull : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] caminantes;
    void Start()
    {
        StartCoroutine(spawnear());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator spawnear()
    {
        while (true)
        {
            for (int i = 0; i < caminantes.Length; i++) 
            {
                if (caminantes[i].gameObject.activeInHierarchy == false)
                {
                    caminantes[i].gameObject.SetActive(true);
                    caminantes[i].transform.position = new Vector3(-10, -4, 0);
                    break;
                }
            }
            yield return new WaitForSeconds(1);
        }
    }
}
