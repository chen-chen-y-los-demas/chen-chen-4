using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class SpawnerOleada : MonoBehaviour
{
    // Start is called before the first frame update
    public ClasseOleadaIndividual[] Oleadas;
    public GameObject NodeManager;
    public GameObject mokInvetory;
    public GameObject textMoney;
    public Camera camera;
    public int nivMoke;
    void Start()
    {
        nivMoke = 0;
        StartCoroutine(spawn());
    }

    private IEnumerator spawn() 
    {
        while (true)
        {
            for (int i = 0; i < Oleadas.Length; i++)
            {
                if (i <= Oleadas.Length - 1)
                {
                    for (int j = 0; j < Oleadas[i].Enemigos.Length; j++)
                    {
                        for (int l = 0; l < Oleadas[i].Enemigos[j].cantidadSpawn; l++)
                        {
                            GameObject enemigoSpawneado = Instantiate(Oleadas[i].Enemigos[j].Enemigo);
                            enemigoSpawneado.GetComponent<Enemigo>().NodeManagerGameObject = this.NodeManager.gameObject;
                            enemigoSpawneado.GetComponent<Enemigo>().mokInventory = this.mokInvetory.gameObject;
                            enemigoSpawneado.GetComponent<Enemigo>().textMoney = this.textMoney.gameObject;
                            enemigoSpawneado.GetComponent<Transform>().position = this.GetComponent<Transform>().position;
                            enemigoSpawneado.GetComponent<Enemigo>().subirNivel(nivMoke);
                            enemigoSpawneado.GetComponent<Enemigo>().canvas.GetComponent<Canvas>().worldCamera = this.camera;
                            enemigoSpawneado.GetComponent<Enemigo>().textoNivel.GetComponent<TextMeshProUGUI>().text = "" + enemigoSpawneado.GetComponent<Enemigo>().nivel;
                            print("instancio");
                            yield return new WaitForSeconds(Oleadas[i].tempSpawn);
                        }
                    }
                    if (i > 15)
                    {
                        yield return new WaitForSeconds(20);
                    }
                    else {
                        yield return new WaitForSeconds(10);
                    }
                    
                    nivMoke++;
                }
            }
        }
    }
}
